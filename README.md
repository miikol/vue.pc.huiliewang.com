# ov_vue

> A Vue.js project

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run e2e tests
npm run e2e

# run all tests
npm test
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
>目录结构：
> |——src
|      |——components 公共组件库
|      |——pages 页面
|      |     |——start 入口
|      |     |——home 首页
|      |     |——customer 客户
|      |     |——business 项目
|      |     |——product 人才
|      |     |——contract 合同
|      |     |——invoice 财务
|      |     |——hleads 报表
|      |     |——integral 职级
|      |     |——background 背调
|      |     |——return 回款
|      |     |——phone 分析模块
|      |     |——setting 系统
|      |——router 路由 路由结构同pages
|      |——filter 全局 filter
|      |——assets 资源库
|           |——css 公共css文件
|           |——icon 图标
|           |——js 公共js文件
|——static 不打包静态资源库
代码规范：