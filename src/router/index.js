import Vue from 'vue'
import Router from 'vue-router'
import home from './home'
import custormer from './customer'
import business from './business'
import product from './product'
import contract from './contract'
Vue.use(Router)

export default new Router({
  routes: [
    {
      path:'/',
      redirect:'/home',
      name:'入口',
      component:()=>import("@/pages/start/index"),
      children:[
        ...home,
        ...custormer,
        ...business,
        ...product,
        ...contract
      ]
    }
  ]
})
