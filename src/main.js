// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './index'
import ElementUI from 'element-ui'//element.ui
import 'element-ui/lib/theme-chalk/index.css'//element.ui
import router from './router'//router
import 'font-awesome/scss/font-awesome.scss'//图标库
import './assets/css/style.css'//公共样式


Vue.config.productionTip = false
Vue.use(ElementUI);
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
